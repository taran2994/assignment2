import com.sun.javafx.scene.traversal.TopMostTraversalEngine;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class AirRideLimo extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		Label headingTextLabel = new Label("Airport Ride Calculator");

		Label fromTextLabel = new Label("From (Brampton / Cestar College) :");

		TextField fromTextBox = new TextField();

		CheckBox luggage = new CheckBox("Extra luggage?");

		CheckBox pets = new CheckBox("Pets?");

		CheckBox etr407 = new CheckBox("Use ETR 407?");

		CheckBox tip = new CheckBox("Add Tip?");

		Button goButton = new Button();
		goButton.setText("CALCULATE");

		Label resultLabel = new Label("");

		goButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				double baseFare = 0.0;
				double additionalCharges = 0.0;
				double tipValue = 0.0;
				double kms = 0.0;
				String namee = fromTextBox.getText();

				if (namee.equalsIgnoreCase("brampton") || namee.equalsIgnoreCase("cestar college")) {
					if (namee.equalsIgnoreCase("Brampton")) {
						baseFare = 38.00;
						kms = 10.0;
					} else if (namee.equalsIgnoreCase("cestar college")) {
						baseFare = 51.00;
						kms = 7.0;
					}
					if (luggage.isSelected()) {
						additionalCharges = additionalCharges + 10;
					}

					if (pets.isSelected()) {
						additionalCharges = additionalCharges + 6;
					}

					if (etr407.isSelected()) {
						additionalCharges = additionalCharges + (kms * 0.25);
					}

					double totalFare = baseFare + additionalCharges;
					double tax = totalFare * 0.13;
					double finalFare = totalFare + tax;

					if (tip.isSelected()) {
						tipValue = finalFare * 0.15;
						finalFare = finalFare + tipValue;
					}

					resultLabel.setText("The total fare is :$" + finalFare);
				}

				else {
					resultLabel.setText("Enter Cestar college or Brampton");
				}
			}
		});

		VBox root = new VBox();

		root.getChildren().add(headingTextLabel);
		root.getChildren().add(fromTextLabel);
		root.getChildren().add(fromTextBox);
		root.getChildren().add(luggage);
		root.getChildren().add(pets);
		root.getChildren().add(etr407);
		root.getChildren().add(tip);
		root.getChildren().add(goButton);
		root.getChildren().add(resultLabel);

		primaryStage.setScene(new Scene(root, 300, 300));
		primaryStage.setTitle("Air Ride Limo");

		primaryStage.show();

	}

}
